FROM docker.io/library/debian:bookworm-slim

ENV DEBIAN_FRONTEND=noninteractive

COPY clickhouse.gpg /usr/share/keyrings/clickhouse.gpg

RUN apt-get -q update \
    && apt-get -y install --no-install-recommends ca-certificates \
    && echo 'deb [signed-by=/usr/share/keyrings/clickhouse.gpg] https://packages.clickhouse.com/deb stable main' > /etc/apt/sources.list.d/clickhouse.list \
    && apt-get -q update \
    && echo "clickhouse-server clickhouse-server/default-password password" | debconf-set-selections \
    && apt-get -y install --no-install-recommends clickhouse-server clickhouse-client \
    && apt-get -q remove --purge -y ca-certificates \
    && apt-get -q autoremove -y \
    && apt-get -q clean \
    && rm -fr /var/lib/apt/lists/* \
    && chown -R root:root /etc/clickhouse* \
    && chmod -R a+rX /etc/clickhouse*

ENTRYPOINT ["/usr/bin/clickhouse-server", "--config=/etc/clickhouse-server/config.xml"]

